import { useState, useEffect, lazy, Suspense } from 'react'
import { Redirect } from 'react-router'

// import ItemListContainer from './components/ItemListContainer/ItemListContainer';
import NavBar from './components/NavBar/NavBar';
import ItemDetailContainer from './components/ItemDetailContainer/ItemDetailContainer';
import Cart from './components/Cart/Cart';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import CartContextProvider from './context/CartContext';
const ItemListContainer = lazy(()=> import('./components/ItemListContainer/ItemListContainer'))


// Eliminar Console.log
// Eliminar Código comentados
// Eliminar librerías que no usemos
// identar bien
// eliminar los import que no se usen
// Eliminar las variables que no se usen
// Eliminar código duplicado 
// separar bien la lógica en subcomponentes
// que cada componente no tenga mas de 100 lineas promedio




//NavBar()

function App() {
    const [estado, setEstado] = useState(true)
    const [count, setCount] = useState(0)
    
    const titulo= 'Soy el componente Form'

    const handleCambiarEstado = ()=>{
        setEstado(!estado)   }

    
    // const handlerClick = ()=>{
    //     alert('soy click del app')
    // }
   
    console.log('App');

    return (
        <CartContextProvider >
            <BrowserRouter>
                <div className="App border border-3 border-primary" 
                    //onClick={handlerClick}
                >
                    <NavBar />
                    <Routes>
                        <Route exact path='/' element={ <Suspense fallback={<h1>Loading..</h1>} ><ItemListContainer /></Suspense>} />            
                        {/* <Route exact path='/' element={ <ItemListContainer />} />             */}
                        <Route exact path='/categoria/:categoryID' element={<ItemListContainer />} />                 
                        <Route exact path='/detail/:id' element={ <ItemDetailContainer />} />
                        <Route exact path='/cart' element={<Cart />} />                                 
                    </Routes>     
                </div>
            </BrowserRouter>
        </CartContextProvider>
    );
}

export default App;
//<button onClick={()=>{setCount(count+1)}} > click </button>

//<button onClick={handleCambiarEstado}>Cambiar estado</button>