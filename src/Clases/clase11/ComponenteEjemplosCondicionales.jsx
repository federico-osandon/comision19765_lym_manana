import { useState, useEffect } from "react";


 
export const ControlledInput = () => {
    const [input, setInput] = useState("");

    useEffect(() => {
      console.log('subcripcion');

      return () => {
        console.log('dessubscripcion')
      }

    }, [input])

    console.log('input');
    console.log(input);
    return (
      <input
        type="text"
        value={input}
        onInput={(evet) => setInput(evet.target.value)}
      />
    );
  };


  
export  function LoadingComponent() {
    const [loading, setLoading] = useState(true);
  
    useEffect(() => {

      setTimeout(() => {
        setLoading(false);
      }, 5000);

      return ()=>{
          console.log('Limpiando componente');
      }
    }, []);
    
    return <>
        {loading ? <h2>Loading... </h2> : <h3>Productos cargardos!</h3>}
    </>;
  }











  
export  function TextComponent({ condition = true }) {
    
    if (!condition) {
      return <h2>Uds no esta logeado</h2>;
    }
  
    return (
        <>
            <h2>Ud esta logueado puede ver la pág.</h2>
        </>
    )
  }








// condicion ? :,  condicion && (if()) , condicion || 


export  function TextComponent2({ condition = true }) {
    
    return (
      <>
        {condition && <h2>Ud esta logueado puede ver la pág.</h2>}

        { !condition && <h2>Ud no esta logueado, NO puede ver la pág.</h2>}

      </>
    );
  }









export  function TextComponent3({ condition = true }) {
    return (
      <>
        <h2> {condition ? 'Ud esta logueado puede ver la pág.' : 'Ud NO esta logueado no puede ver la pág.'} </h2>            
        
      </>
    )
  }









 export function TextComponent4({ condition = true }) {

    return (
      <>
        <h2 style={ { color: condition ? "green" : "red" } }>
          Ud esta logueado puede ver la pág.
        </h2>
      </>
    );
  }













  
export  function TextComponent5({ condition = true }) {
    return (
      <>
        <h2 className={ (condition === true) ? "btn btn-success" : "btn btn-danger" }>
         stock
        </h2>
      </>
    );
  }

















export  function TextComponent6( { condition = true , otro = 'mt-5' }  ) {
    return (
      <>
        <h2
          className={ `${condition === true ? "btn btn-success" : "btn btn-danger"} ${ otro || "" } `}
        >
          Ud esta logueado puede ver la pág.
        </h2>
      </>
    );
  }












export function TextComponent7({ condition = false , otro = "mt-5" }) {
    
    const config = condition

      ?
            {
                className: `btn btn-success ${otro || ""}`,
                style: {color: 'red'},
                title: "Este es el titulo si la condicion es verdadera",
                nombre: 'Fede'
            }
      : 
            {
                className: `btn btn-warning ${otro || ""}`,
                style: {color: 'green'},
            }

            
    
      return (
      <>
        <h2 {...config} >Ud esta logueado puede ver la pág.</h2>
      </>
    )
  }