import {createContext, useState, useContext} from 'react'

const CartContext = createContext([])

export const useCartContext = ()=>{
    return useContext(CartContext)
}



const CartContextProvider = ({children}) => {
    const [cartList, setCartList] = useState([])

    
    // const agregarItem = (item) => {

    //     const isInCart = (item) => {
    //         return cartList.some((cartItem) => cartItem.id === item.id);
    //     };

    //     if (isInCart(item)) {
    //         let newCart = cartList

    //         newCart.forEach((i) => {
    //             if (i.id === item.id) {
    //                 i.cantidad += item.cantidad
    //             }
    //         });            
    //         setCartList(newCart)
    
    //     } else {
    //         setCartList([...cartList, { ...item }])
    //     }

    // }

     const agregarCarrito = (item) => {

        const index = cartList.findIndex(i => i.id === item.id)//pos    -1
  
          if (index > -1) {
            const oldQy = cartList[index].cantidad
  
            cartList.splice(index, 1)
            setCartList([...cartList, { ...item, cantidad: item.cantidad + oldQy}])
          } else {
            setCartList([...cartList, {...item, cantidad: item.cantidad}])
          }
      }


    // function agregarCarrito(item) {
    //     setCartList( [...cartList , item] )
    // }

    const precioTotal =()=>{
        return cartList.reduce((acum, valor)=>(acum + (valor.cantidad * valor.price)), 0) 
    }



    const borrarCart=()=>{
        setCartList([])
    }

    const cantidadItem = () =>{
        return cartList.reduce( (acum, item)=> acum = acum + item.cantidad , 0)
    }


    return (
        <CartContext.Provider value={{
            cartList,
            agregarCarrito,
            borrarCart,
            cantidadItem,
            precioTotal
        }} >
            {children}
        </CartContext.Provider>
    )
}

export default  CartContextProvider
