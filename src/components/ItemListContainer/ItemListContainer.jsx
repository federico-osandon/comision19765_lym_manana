import {useState, useEffect} from 'react'
import { useParams } from 'react-router-dom'
import { getFetch } from '../../helpers/getFetch'
import { getFirestore } from '../../service/getFirestore'
import ItemList from '../ItemList/ItemList'



const ItemListContainer = () => {
    const [bool, setBool] = useState(true)
    const [product, setProduct] = useState([])
    const [prod, setProd] = useState({})
    const [loading, setLoading] = useState(true)

    const { categoryID } = useParams()

    useEffect(() => {
            const dbQuery = getFirestore() // conexion con firestore

            // dbQuery.collection('items').doc('o0SRMKuaZ1nik6jwLDQ3').get() // traer uno por el id
            // .then(resp => setProd( { id: resp.id, ...resp.data() } ))

            if (categoryID) {
                dbQuery.collection('items').where('categoria', '==', categoryID).get() // traer todo
                .then(data => setProduct(   data.docs.map(pro => ( { id: pro.id, ...pro.data() } ))   ))
                .catch(err=> console.log(err))
                .finally(()=> setLoading(false))    

            } else {                
                dbQuery.collection('items').get() // traer todo
                .then(data => setProduct(   data.docs.map(pro => ( { id: pro.id, ...pro.data() } ))   ))
                .catch(err=> console.log(err))
                .finally(()=> setLoading(false))
            }

            // if (categoryID) {
            //     getFetch
            //     .then( res => {        
            //         console.log('llamada a api') // alguna accion con la respuesta  
            //         setProduct(res.filter(prod => prod.categoria === categoryID))
            //     })    
            //     .catch(err => console.log(err))
            //     .finally(()=> setLoading(false))  
                
            // }else{
            //     getFetch
            //     .then( res => {        
            //         console.log('llamada a api') // alguna accion con la respuesta  
            //         setProduct(res)
            //     })    
            //     .catch(err => console.log(err))
            //     .finally(()=> setLoading(false))  
            // }
        
    }, [categoryID]) 



    //ejemplo de evento
    const handleClick=(e)=>{
        e.preventDefault() 
        setBool(!bool)
    }

    const handleAgregar=()=>{
        setProduct([
            ...product,
            { id: "8", name: "Gorra 7", url: 'https://www.remerasya.com/pub/media/catalog/product/cache/e4d64343b1bc593f1c5348fe05efa4a6/r/e/remera_negra_lisa.jpg', categoria: "remera" , price: 2 }
        ])
    }
      
    //console.log(bool);
    console.log(product);
    //console.log('itemListContainer');

    return (
        <> 
            <button onClick={handleClick}>Cambiar estado </button>           
            <button onClick={handleAgregar}>Agregar Item </button> 
            {!loading ? 
                    <ItemList product={product}/>     
                    :             
                    <h2>Cargando...</h2>            
            }
        </>
    )
}

export default ItemListContainer

