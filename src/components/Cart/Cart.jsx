
import {useState} from 'react'
import { useCartContext } from '../../context/CartContext'
import firebase from "firebase"
import 'firebase/firestore'
import { getFirestore } from '../../service/getFirestore'



const Cart = () => {
    const [idOrder, setIdOrder] = useState('')

    const [formData, setFormData] = useState({
        name:'',
        phone:'',
        email: ''
    })
 

    const { cartList, borrarCart, precioTotal } = useCartContext()


    const generarOrden = (e) =>{
        
        e.preventDefault()        
        const orden = {}

        orden.date = firebase.firestore.Timestamp.fromDate(new Date());    

        orden.buyer = {nombre: 'Fede', email:'f@gmail.com', tel: '21346546'}
        orden.total =  precioTotal()

        orden.items = cartList.map(cartItem => {
            const id = cartItem.id
            const nombre = cartItem.nombre
            const precio = cartItem.price * cartItem.cantidad

            return {id, nombre, precio}   
        })
        
        const dbQuery = getFirestore()  
        dbQuery.collection('orders').add(orden)
        .then(resp => console.log(`su orden de compra es : ${resp.id}`))
        .catch(err=> console.log(err))


        // dbQuery.collection('items').doc('o0SRMKuaZ1nik6jwLDQ3').update({
        //     stock: 9
        // })
       
    
    
        //Actualiza todos los items que estan en el listado de Cart del CartContext
    
        const itemsToUpdate = dbQuery.collection('items').where(
            firebase.firestore.FieldPath.documentId() , 'in', cartList.map(i=> i.id)
        )
    
        const batch = dbQuery.batch();
        
        // por cada item restar del stock la cantidad de el carrito
    
        itemsToUpdate.get()

        .then( collection=>{
            collection.docs.forEach(docSnapshot => {
                batch.update(docSnapshot.ref, {
                    stock: docSnapshot.data().stock - cartList.find(item => item.id === docSnapshot.id).cantidad
                })
            })
    
            batch.commit().then(res =>{
                console.log('se actualizo')
            })
        })
    
        
          console.log('verificar cupon')
          console.log(orden)
    }
 
    const handleChange=(e)=>{
       setFormData({
            ...formData, 
            [e.target.name]: e.target.value
        })
    }
    console.log(formData)
 
    return (
        <div>
            <h1>Hola soy Cart</h1>
            <section>
                {idOrder!==''&& <label>El id de su orden es : {idOrder}</label>}
            </section>
            {cartList.map(prod => <li>{prod.nombre} { prod.cantidad}</li>)}
            {`Precio total: ${precioTotal()}`}
            <button onClick={ ()=> borrarCart()} >borrar el carrito</button>
            <form 
                onSubmit={generarOrden} 
                // onChange={handleChange} 
            >
                {/* <input type='text' name='name' placeholder='name' value={formData.name}/>
                <input type='text' name='phone'placeholder='tel' value={formData.phone}/>
                <input type='email' name='email'placeholder='email' value={formData.email}/> */}
                <button>Enviar</button>
            </form>

        </div>
    )
}

export default Cart
