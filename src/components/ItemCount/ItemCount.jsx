import {useState} from 'react'
import { Link } from 'react-router-dom'

const ItemCount = ({initial, stock, onAdd}) => {
    const [count, setCount] = useState(1)
    const [condicionBoton, setCondicionBoton] = useState(true)

    function Sumar(){
        if (count<stock) {
            setCount(count + 1)            
        }
    }
    function Restar(){
        if (count>initial) {
            setCount(count - 1)            
        }
    }
    function Agregar(){
        onAdd(count)
        setCount(1)
        setCondicionBoton(false)
    }

    return (
        <>
            <section>
                <button onClick={Sumar}>+</button>
                    {count}
                <button onClick={Restar}>-</button>
                {condicionBoton ? 
                        <button onClick={Agregar} >Agregar al carrito</button> 
                    : 
                        <>
                            <Link to='/'>
                                <button>Seguir Comprando</button> 
                            </Link>
                            <Link to='/cart'>
                                <button>Terminar Compra</button> 
                            </Link>
                        </>
                }
                

            </section> 
        </>
    )
}

export default ItemCount
