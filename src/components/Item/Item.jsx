import { Link } from 'react-router-dom'

const Item = ({prod={}}) => {
    console.log('item');
    return (
        <div 
            className='col-md-4'
        >                        
            <div className="card w-100 mt-5" >
                <div className="card-header">
                    {`${prod.nombre} - ${prod.categoria}`}
                </div>
                <div className="card-body">
                    <img src={prod.urlImagen} alt='' className='w-50' />
                    {prod.price}                                                            
                </div>
                <div className="card-footer">
                        <Link to={`/detail/${prod.id}`}>
                            <button className="btn btn-outline-primary btn-block">
                                detalle del producto
                            </button>
                        </Link>                                                            
                </div>
            </div>
        </div>
    )
}

export default Item
