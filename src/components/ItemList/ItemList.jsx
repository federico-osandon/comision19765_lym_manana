import {memo} from 'react'
import Item from "../Item/Item"
//memo(fn-componente) memo(fcComponente, fnCondición)
const ItemList = memo(
    ({product}) => {
        console.log('ItemList');
        return (
            <div className='container'>
                        <div className="row" dataMansory={{percentPosition: true}}>
                            { product.map(prod=>  <Item key={prod.id} prod={prod} />  )}
                        </div>
                    </div>      
        )
    }
)

export default ItemList
