import React from 'react'
import ImgCart from './cart.svg'

const CartWidget = () => {
    return (
        <>
            <img src={ImgCart} alt='imagén de carrito' />
        </>
    )
}

export default CartWidget
