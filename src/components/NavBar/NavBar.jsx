import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';

import CartWidget from '../CartWidget/CartWidget';

import { Link } from 'react-router-dom'
import { useCartContext } from '../../context/CartContext';

const NavBar=()=>{

    const { cantidadItem } = useCartContext()
    return(
        <Navbar bg="light" expand="lg">
            <Container fluid>
                <Navbar.Brand href="#">Navbar scroll</Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                <Nav
                    className="me-auto my-2 my-lg-0"
                    style={{ maxHeight: '100px' }}
                    navbarScroll
                >
                    <Link to="/">Home</Link>
                    <Link to="/categoria/gorras">Gorras</Link>                  
                    <Link to="/categoria/remeras">Remeras</Link>                    
                </Nav>
                <Link to='/cart'>
                    { cantidadItem() !== 0 && cantidadItem() }
                    <CartWidget />
                </Link>
                </Navbar.Collapse>
            </Container>
            </Navbar>
    )
}
export default NavBar