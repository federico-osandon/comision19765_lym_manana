import {useState, useEffect} from 'react'
import {useParams} from 'react-router-dom'
import { getFirestore } from '../../service/getFirestore'
import ItemDetail from './ItemDetail'

const ItemDetailContainer = () => {
    const [loading, setLoading] = useState(true)
    const [prod, setProd] = useState({})
    const {id} = useParams()


    useEffect(() => {
        const db = getFirestore()
        db.collection('items').doc(id).get()
        .then( res => {        
            console.log('llamada a api') // alguna accion con la respuesta  
            setProd( {id: res.id, ...res.data()} )
        })    
        .catch(err => console.log(err))
        .finally(()=> setLoading(false))
        
        // eslint-disable-next-line       
    },[]) 
      
    console.log(prod);

    return (
        <>
            {loading ? 
                    <h2>Cargando...</h2>
                :
                    <div className='border border-3 border-secondary'>
                        <ItemDetail producto={prod} />                        
                    </div>
            }
        </>
    )
}

export default ItemDetailContainer
