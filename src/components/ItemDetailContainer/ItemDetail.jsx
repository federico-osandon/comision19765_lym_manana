import {useEffect, useState} from 'react'
import { Col, Row } from "react-bootstrap"
import { useCartContext } from '../../context/CartContext'
import ItemCount from '../ItemCount/ItemCount'



const ItemDetail = ({producto}) => {
    const [count, setCount] = useState(0)

    const {cartList, agregarCarrito } = useCartContext()
  
    function onAdd(cant){
        setCount(cant) 
        agregarCarrito( {...producto, cantidad: cant} ) 
        console.log(`la cantidad seleccionada es ${cant}`)      
    }
    console.log(cartList)

    return (
        <Row>
                <label>Soy el detalle</label>
                <Col>                
                    <div className='card w-50'>
                        <div className="container">
                            <label>{producto.nombre}</label>
                        </div>
                        <div className="container">
                            <img  src={producto.urlImagen} className="w-25" alt="foto" />
                            <br/>
                            <label>{producto.descripcion}</label>
                        </div>
                        <div className="container">
                            <label>{producto.price}</label>
                        </div>
                    </div>
                </Col>
                <Col>

                   <ItemCount initial={1} stock={5} onAdd={onAdd} />                                      
                </Col>
            </Row>
    )
}

export default ItemDetail
